# Charla Ionic y AngularJS 
##### En este documento queda plasmado todo el **material,enlaces y ejemplos** de la charla realizada el Miercoles 27 de Agosto.

## AngularJS

### Ejemplos
* [Javascript vs AngularJS](http://jsfiddle.net/hernangiraldo89/beds9dca/)
* [jQuery vs AngularJS](http://jsfiddle.net/hernangiraldo89/wv7uf4ao/1/)
* [Data Binding](http://jsfiddle.net/hernangiraldo89/56argj1w/5/)
* [Creación de Directivas](http://jsfiddle.net/hernangiraldo89/kb2e6be3/1/)
* [Filtros](http://jsfiddle.net/hernangiraldo89/p8wzsw6o/1/)
* [Controladores](http://jsfiddle.net/hernangiraldo89/jh9jdbr4/5/)
* [Servicios](http://jsfiddle.net/hernangiraldo89/L05r8fkc/2/)
* [Rutas](http://plnkr.co/edit/yNWkbg?p=preview)



### Live Coding
Ejemplo realizado al final de la charla. Aparte de usar [AngularJS](https://angularjs.org/), se uso un framework de HTML, CSS y JS llamado [Bootstrap](http://getbootstrap.com/) para darle estilo a la página.

* [Live Coding](http://plnkr.co/edit/eWeHKdMUR5b5QX40IdOU?p=preview)

## Ionic
* [CSS](http://ionicframework.com/docs/components/)
* [JavaScript](http://ionicframework.com/docs/api/)

### Bibliografía
1. **Libro:** JavaScript: The Good Parts - **Autor:** Douglas Crockford
2. **Libro:** AngularJS - **Autores:** Brad Green & Shyan Seshadri
3. **Libro:** Mastering Web Application Development With AngularJS - **Autores:** Peter Bacon Darwin & Pawel Kozlowski



### Video
En esta charla **Douglas Crockfor** expondrá las bondades de JavaScript, un lenguaje de programación dinámico excepcional. Dentro del lenguaje hay un 'subset' elegante que es muy superior al lenguaje como un todo, siendo más confiable, fácil de leer y de mantener.

[![JavaScript: The Good Parts](http://img.youtube.com/vi/lP9-Zx_cCUg/0.jpg)](http://www.youtube.com/watch?v=lP9-Zx_cCUg)

_click en la imagen para ir al video_


### Video (AngularJS Webinar)
[AngularJS, una introducción en español](https://www.youtube.com/watch?v=qL89obEtgko)


### Cursos
**JavaScript**
* [CodeSchool](https://www.codeschool.com/courses/javascript-road-trip-part-1)
* [CodeAcademy](http://www.codecademy.com/es/tracks/javascript-latinamerica)

**jQuery**
* [CodeSchool](https://www.codeschool.com/courses/try-jquery)
* [CodeAcademy](http://www.codecademy.com/es/tracks/jquery-latinamerica)

**AngularJS**
* [CodeSchool](https://www.codeschool.com/courses/shaping-up-with-angular-js)

**Git**
* [TryGit](https://www.codeschool.com/courses/try-git)


### Airbnb JavaScript Style Guide
* [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)



### Principles of Writing Consistent, Idiomatic JavaScript
* [Principles of Writing Consistent, Idiomatic JavaScript](https://github.com/rwaldron/idiomatic.js/)



### Stric Mode
* ["use strict";](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions_and_function_scope/Strict_mode)


### Karma y Jasmine

##### Paginas de los frameworks
* [Karma](http://karma-runner.github.io/0.12/index.html)
* [Jasmine](http://jasmine.github.io/2.0/introduction.html)


##### Instalación y configuración de karma y jasmine
* [Tutorial instalacion y configuracion](https://www.youtube.com/watch?v=6TsDRP0ayRU)
* [Tutorial instalacion y configuracion](https://www.youtube.com/watch?v=tTD9yLCipuM)

##### Documentación de karma con Angularjs
* [Documentacion](http://www.yearofmoo.com/2013/01/full-spectrum-testing-with-angularjs-and-karma.html)
* [Charla](https://www.youtube.com/watch?v=rB5b67Cg6bc)



### Presentación
* [Presentación AngularJS](https://docs.google.com/a/pragma.com.co/presentation/d/1v6J6-3MGmCzrBco7yw4VOM2w0gn033C49Po3a22ioMs/edit#slide=id.g37ad8a555_411)




### Reto

El reto que deben cumplir es adicionar al ejemplo visto en la charla, un 'Prodiver' que administre los datos y usar el enrutamiento con 'Routes' de AngularJS para mostrar varias vistas. 


***
_Denis Monsalve_ - _Hernán Giraldo_ - _Santiago Cuervo_